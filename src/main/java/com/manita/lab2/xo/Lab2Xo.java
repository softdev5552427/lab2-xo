/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.manita.lab2.xo;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class Lab2Xo {

    static String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    public static String turn = "X";
    public static boolean confirm;
    public static int row;
    public static int col;

    public static void printStartGame() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to XO Game");
        System.out.println("Start XO Games? (Y/N): ");
        String start = sc.nextLine().toUpperCase();
        while (!start.equals("Y") && !start.equals("N")) {
            System.out.println("Start XO Games? (Y/N): ");
            start = sc.nextLine().toUpperCase();
        }
        if (start.equals("N")) {
            confirm = false;
            System.out.println("Goodbye!!!");
        } else {
            confirm = true;
        }
    }

    public static void printTurn() {
        System.out.println("Turn: " + turn);
    }

    public static void printTable() {
        for (int i = 0; i < table.length; i++) {
            for (int y = 0; y < table.length; y++) {
                System.out.print(" " + table[i][y] + " ");
            }
            System.out.println();
        }
    }

    public static void inputRowAndCol() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input row: ");
        row = sc.nextInt();
        System.out.println("Input column: ");
        col = sc.nextInt();
        while ((!(row > 0 && row < 4) || !(col > 0 && col < 4))) {
            System.out.println("that row and Column is already exits.");
            System.out.print("Input row :");
            row = sc.nextInt();
            System.out.print("Input column :");
            col = sc.nextInt();
        }
        if (((row > 0 && row < 4) && (col > 0 && col < 4))) {
            table[row - 1][col - 1] = turn;
        }

    }

    public static void changeTurn() {
        if (turn.equals("X")) {
            turn = "O";
        } else {
            turn = "X";
        }
    }

    public static boolean checkRow() {
        for (int j = 0; j < table[row - 1].length; j++) {
            if (!table[row - 1][j].equals(turn)) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkColumn() {
        for (int i = 0; i < table[row - 1].length; i++) {
            if (!table[i][col - 1].equals(turn)) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX() {
        if (row - 1 == col - 1) {
            for (int i = 0; i < table.length; i++) {
                if (!table[i][i].equals(turn)) {
                    return false;
                }

            }
            return true;
        }
        if ((row + col) - 2 == table.length - 1) {
            for (int i = 0; i < table.length; i++) {
                if (!table[i][table.length - 1 - i].equals(turn)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static boolean checkDraw() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                if (table[i][j].equals("-")) {
                    return false;
                }

            }

        }
        return true;
    }

    public static void reset() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                table[i][j] = "-";
            }
        }
    }

    public static boolean checkWin() {
        if (checkRow() || checkColumn() || checkX()) {
            System.out.println("+--------------------+");
            System.out.println("|    !!! " + turn + " Win !!!   |");
            System.out.println("+--------------------+");
            changeTurn();
            return true;

        }
        if (checkDraw()) {
            System.out.println("+--------------------+");
            System.out.println("|    !!! Draw !!!    |");
            System.out.println("+--------------------+");
            changeTurn();
            return true;
        }
        return false;
    }

//    public static void printWinOrDraw() {
//        if (checkRow() || checkColumn() || checkX()) {
//            System.out.println("+--------------------+");
//            System.out.println("|    !!! " + turn + " Win !!!   |");
//            System.out.println("+--------------------+");
//        }
//        if (checkDraw()) {
//            System.out.println("+--------------------+");
//            System.out.println("|    !!! Draw !!!    |");
//            System.out.println("+--------------------+");
//        }
//    }
    public static boolean printPlayAgain() {
        System.out.print("You want to play again (Y/N) : ");
        Scanner sc = new Scanner(System.in);
        String again = sc.nextLine().toUpperCase();
        while (!again.equals("N") && !again.equals("Y")) {
            System.out.print("You want to play again (Y/N) : ");
            again = sc.nextLine().toUpperCase();
        }
        if (again.equals("N")) {
            confirm = false;
            System.out.println("GoodBye!!");
            return false;

        }

        return true;
    }

    public static void main(String[] args) {
        printStartGame();
        while (confirm == true) {
            printTurn();
            printTable();
            inputRowAndCol();
            if (checkWin() == true) {
                printTable();
//                printWinOrDraw();
                printPlayAgain();
                reset();
            }
            changeTurn();
        }
    }
}
